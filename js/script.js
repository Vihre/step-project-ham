//вкладки в Our Services
let tabsTitlesService = document.getElementsByClassName('tabs__titles-service')[0];
let tabsContentService = document.getElementsByClassName('tabs__content-service')[0];
tabsTitlesService.onclick = function (event) {
   if (event.target.closest('.tab__title-service')) {
      Array.from(tabsTitlesService.children).forEach(title => title.classList.remove('active-service-tab'));
      event.target.classList.add('active-service-tab');

      Array.from(tabsContentService.children).forEach(tab => tab.dataset.service === event.target.dataset.service
         ? tab.classList.add('active-service-tab')
         : tab.classList.remove('active-service-tab'));
   }
};

//вкладки в Our Amazing Work
let tabsTitlesWork = document.querySelector('.tabs__titles-work');
let tabsContentWork = document.querySelector('.tabs__content-work');
tabsTitlesWork.addEventListener('click', addWorkItem);
let imgWorkCount = 1;
let itemsCount = 12;

function addWorkItem(event) {
   let itemType = event.target.dataset.work;
   Array.from(tabsTitlesWork.children).forEach(title => title.classList.remove('active-work-title'));
   event.target.classList.add('active-work-title');

   Array.from(tabsContentWork.children).forEach(tab => tab.classList.remove('active-work-tab'));
  
   if (itemType.toLowerCase() === 'all' && Array.from(tabsContentWork.children).length >= itemsCount) {
      for (let i = 0; i < itemsCount; i++) {
         Array.from(tabsContentWork.children)[i].classList.add('active-work-tab')
      }
   } else {
      if (itemType.toLowerCase() === 'all') {
         itemType = Array.from(tabsContentWork.children)[0].dataset.workItem;
      };
      let arrWorkItems = document.querySelectorAll(`[data-work-item="${itemType}"]`);
      arrWorkItems.forEach(item => item.classList.add('active-work-tab'));
      if (arrWorkItems.length < itemsCount) {
         for (let i = arrWorkItems.length; i < itemsCount; i++) {
            let randomImg = `https://picsum.photos/300/220?random=${imgWorkCount}`
            tabsContentWork.insertAdjacentHTML("beforeend",
               `<div data-work-item="${itemType}" class="tab__item-work active-work-tab new-work-tab">
               <img src=${randomImg} alt="work example" />
               <div class="details__item-work">
                  <div class="details__item-work-btns">
                     <button class="details__item-work-btn"></button>
                     <button class="details__item-work-btn"></button>
                  </div>
                  <h4>creative design</h4>
                  <h5>${itemType}</h5>
               </div>
               </div>`);
            imgWorkCount++
         };
      }
   }
};

//button ADD in Our Amazing Work 
let workBtn = document.querySelector('.work__btn');
let workBtnClickCount = 0;
let workBtnTimerId;

workBtn.onclick = function () {
   workBtn.setAttribute("disabled", '');
   workBtn.innerHTML = `<div class="btn__loadrer-container">
                           <span class="circle"></span>
                           <span class="circle"></span>
                           <span class="circle"></span>
                           <span class="circle"></span>
                        </div>`;
   workBtn.style = "background-color: #14B9D5;"
   workBtnTimerId = setTimeout(() => {
      itemsCount += 12;
      document.querySelector('.active-work-title').click();
      if (workBtnClickCount === 1) {
         document.querySelector('.work').style = "padding-bottom: 99px;"
         workBtn.remove();
         clearTimeout(workBtnTimerId);
      }
      workBtnClickCount++
      workBtn.innerHTML = '<p>Load More</p>'
      workBtn.style = '';
      workBtn.removeAttribute("disabled");
   }, 2000);
};

//masonry in Gallery of best images
let gallery = document.querySelector('.gallery__items');
let msnry = new Masonry(gallery, {
   itemSelector: '.gallery__item',
   columnWidth: 10
});
//перевірка на заівнтаження всіх картинок
imagesLoaded(gallery).on('progress', function () {
   msnry.layout();
});


// ADD button in Gallery of best images
let galleryBrn = document.querySelector('.gallery__btn');
let galleryBrTimerId;
let imgGalleryCount = 1;

galleryBrn.addEventListener('click', addGalleryItem);

function addGalleryItem() {
   galleryBrn.setAttribute("disabled", '');
   galleryBrn.innerHTML = `<div class="btn__loadrer-container">
                           <span class="circle"></span>
                           <span class="circle"></span>
                           <span class="circle"></span>
                           <span class="circle"></span>
                        </div>`;
   galleryBrn.style = "background-color: #14B9D5;";

   galleryBrTimerId = setTimeout(() => {
      for (let i = 0; i < 12; i++) {
         let randomImg = `https://picsum.photos/900/900?random=${imgGalleryCount}`;
         gallery.insertAdjacentHTML("beforeend",
            `<div class="gallery__item size1">
            <div class="gallery__item-hover">
               <button class="gallery__item-hover--rect rect1">
                  <i class="fa-solid fa-magnifying-glass"></i>
               </button>
               <button class="gallery__item-hover--rect rect2">
                  <i class="fa-solid fa-maximize"></i>
               </button>
            </div>
            <img src="${randomImg}" alt="galary example">
         </div>`);
         imgGalleryCount++
      };

      galleryBrn.innerHTML = '<p>Load More</p>';
      galleryBrn.style = '';
      galleryBrn.removeAttribute("disabled");

      let masonry = new Masonry(gallery, {
         itemSelector: '.gallery__item',
         columnWidth: 10
      });
       imagesLoaded(gallery).on('progress', function () {
         masonry.layout();
      });
   }, 2000);
};

//SWIPER
let menu = [`<img src="./img/Layer 344.png" alt="employee photo">`,
   `<img src="./img/Layer 6.png" alt="employee photo">`,
   `<img src="./img/Layer 5.png" alt="employee photo">`,
   `<img src="./img/Layer 7.png" alt="employee photo">`,];

const swiper = new Swiper('.swiper', {
   loop: true,

   pagination: {
      el: '.feedback__slider-photos',
      type: 'bullets',
      bulletClass: 'slider-photo',
      bulletActiveClass: 'active-slider',
      clickable: true,
      clickableClass: 'feedback__slider-photos',
      renderBullet: function (index, className) {
         return '<button class="' + className + '">' + (menu[index]) + '</button>';
      },
   },
   navigation: {
      nextEl: '.right-btn',
      prevEl: '.left-btn',
   },
});
swiper.slideTo(3, 100, false);

